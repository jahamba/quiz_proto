using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PhotoView : MonoBehaviour
{
    [SerializeField] private Image image;
    
    public PhotoItem photoItem;
    public int index;

    public void SetPhotoItem(PhotoItem item)
    {
        photoItem = item;
        image.sprite = item.Sprite;
    }
}
