using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class Sticker : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] private Image image;
    private PhotoItem _photoItem;
    
    private static List<PhotoItem> _photoItems;

    private static List<PhotoItem> PhotoItems
    {
        get
        {
            if (_photoItems == null)
            {
                _photoItems = 
                    Resources.LoadAll<Sprite>("Photos").Select(sprite => new PhotoItem(sprite)).
                        OrderBy(x => Random.value).ToList();
            }

            return _photoItems;
        }
    }

    private static List<int> _usedNumbers = new List<int>();
    private static List<int> _numbersOnScreen = new List<int>();

    private static List<Sticker> _stickers = new List<Sticker>();

    private void Awake()
    {
        _stickers.Add(this);
        SetItem();
    }

    private static void UpdateStickers(int limit = -1)
    {
        _numbersOnScreen.Clear();
        foreach (var sticker in _stickers)
        {
            sticker.SetItem(limit);
        }
    }

    public static void ClearUsedNumbers() => _usedNumbers.Clear();
    
    private void SetItem(int limit=-1)
    {
        while (true)
        {
            var item = PhotoItems[Random.Range(0, PhotoItems.Count)];
            if (limit > 0 && item.Number > limit) continue;
            if(_usedNumbers.Contains(item.Number) || _numbersOnScreen.Contains(item.Number)) continue;

            _numbersOnScreen.Add(item.Number);
            image.sprite = item.Sprite;
            _photoItem = item;
            break;
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        StartCoroutine(StickerClick());

        IEnumerator StickerClick()
        {
            var t = transform;
            t.SetAsLastSibling();
            const float speed = 5f;
            var sourcePosition = t.position;
            var targetPosition = NumberView.NextEmpty().transform.position;

            var otherStickers = _stickers.Where(sticker => sticker != this).ToList();
            var targetColor = new Color(1f, 1f, 1f, 0f);
            
            for (var delta = 0f; delta < 1; delta+=Time.deltaTime * speed)
            {
                foreach (var sticker in otherStickers)
                {
                    sticker.image.color = Color.Lerp(Color.white, targetColor, delta);
                }
                
                yield return null;
            }
            
            foreach (var sticker in otherStickers)
            {
                sticker.image.color = targetColor;
            }
            
            for (var delta = 0f; delta < 1; delta+=Time.deltaTime * speed)
            {
                t.position = Vector3.Lerp(sourcePosition, targetPosition, delta);
                t.localScale = Vector3.Lerp(Vector3.one, Vector3.zero, delta);

                yield return null;
            }
            
            t.localScale = Vector3.zero;

            yield return NumberView.SetNumberStatic(_photoItem.Number);
            _usedNumbers.Add(_photoItem.Number);
            
            t.position = sourcePosition;
            t.localScale = Vector3.one;
            
            foreach (var sticker in otherStickers)
            {
                sticker.image.color = Color.white;
            }

            
            if (NumberView.NextEmpty() != default)
            {
                UpdateStickers(NumberView.NextEmptyIndex() < 5 ? -1 : 10);
                var stickerTransforms = _stickers.Select(x => x.transform).ToList();
                var stickerTargetPositions = stickerTransforms.Select(x => x.localPosition).ToList();

                for (var delta = 0f; delta < 1; delta += Time.deltaTime * speed)
                {
                    for (var i = 0; i < stickerTransforms.Count; i++)
                    {
                        stickerTransforms[i].localPosition = Vector3.Lerp(
                            stickerTargetPositions[i] + Vector3.right * Screen.width,
                            stickerTargetPositions[i],
                            delta);
                    }

                    yield return null;
                }

                for (var i = 0; i < stickerTransforms.Count; i++)
                {
                    stickerTransforms[i].localPosition = stickerTargetPositions[i];
                }
            }
            else
            {
                _stickers.First().transform.parent.gameObject.SetActive(false);

                yield return ResultFrame.Show();
            }
        }
    }
}
