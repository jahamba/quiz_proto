using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class BuyButton : MonoBehaviour
{
    private void Awake()
    {
        gameObject.SetActive(false);
    }

    private int _numberOfTickets = 0;
    
    public void Show()
    {
        _numberOfTickets++;

        var label = GetComponentInChildren<TextMeshProUGUI>();
        label.text = "Оплатить " + _numberOfTickets + " билет";
        
        if (gameObject.activeSelf) return;
        
        gameObject.SetActive(true);
        StartCoroutine(ShowCoroutine());

        IEnumerator ShowCoroutine()
        {
            var t = transform;
            var sourcePosition = t.localPosition + Vector3.down * 120;
            var targetPosition = t.localPosition;
            var speed = 6f;
            for (var delta = 0f; delta < 1; delta+=Time.deltaTime * speed)
            {
                t.localPosition = Vector3.Lerp(sourcePosition, targetPosition,delta);
                yield return null;
            }
            
            t.localPosition = targetPosition;
        }
    }
    
    
}
