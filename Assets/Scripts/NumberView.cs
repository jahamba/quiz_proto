using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class NumberView : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] private TextMeshProUGUI textMeshProUGUI;
    [SerializeField] private Image background;

    private static List<NumberView> _numberViews = new List<NumberView>();

    private int _number;
    private void Awake()
    {
        _numberViews.Add(this);
        _numberViews = _numberViews.OrderBy(x => x.name).ToList();
        SetEmpty();
    }

    public static NumberView NextEmpty() =>
        _numberViews.FirstOrDefault(numberView => numberView._number <= -1);

    public static int NextEmptyIndex()
    {
        for (var i = 0; i < _numberViews.Count; i++)
        {
            if (_numberViews[i]._number < 0) return i;
        }

        return -1;
    }


    public static Coroutine SetNumberStatic(int number) =>
        NextEmpty().SetNumber(number);
    

    public Coroutine SetNumber(int number)
    {
        return StartCoroutine(SetNumberCoroutine());

        IEnumerator SetNumberCoroutine()
        {
            const float speed = 7f;
            var backgroundSourceColor = background.color;
            var backgroundTargetColor = Color.white;
            
            var textSourceColor = textMeshProUGUI.color;
            var textTargetColor = new Color(0f, 0f, 0f, 0.65f);

            var t = transform;
            var peekScale = Vector3.one * 1.5f;
            
            for (var delta = 0f; delta < 1; delta += Time.deltaTime * speed)
            {
                background.color = Color.Lerp(backgroundSourceColor, backgroundTargetColor, delta);
                textMeshProUGUI.color = Color.Lerp(textSourceColor, textTargetColor, delta);
                t.localScale = Vector3.Lerp(Vector3.one, peekScale, delta);
                yield return null;
            }

            background.color = backgroundTargetColor;
            textMeshProUGUI.text = number.ToString();
            textMeshProUGUI.color = textTargetColor;
            _number = number;
            
            for (var delta = 0f; delta < 1; delta += Time.deltaTime * speed)
            {
                t.localScale = Vector3.Lerp(peekScale, Vector3.one, delta);
                yield return null;
            }
            
        }
        
    }

    public void SetEmpty()
    {
        background.color = new Color(1f, 1f, 1f, 0.3f);
        textMeshProUGUI.color = new Color(0f, 0f, 0f, 0.13f);
        textMeshProUGUI.text = "?";
        _number = -1;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        SetEmpty();
    }

    public static void EmptyAll()
    {
        foreach (var numberView in _numberViews)
        {
            numberView.SetEmpty();
        }
    }
}
