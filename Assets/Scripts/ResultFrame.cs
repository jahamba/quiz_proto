using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResultFrame : MonoBehaviour
{
    private static ResultFrame _instance;
    private void Awake()
    {
        _instance = this;
        gameObject.SetActive(false);
    }

    public static Coroutine Show()
    {
        _instance.gameObject.SetActive(true);
        return _instance.StartCoroutine(ShowCoroutine());

        IEnumerator ShowCoroutine()
        {
            var t = _instance.transform;
            var targetPosition = t.localPosition;
            var sourcePosition = targetPosition + Vector3.down * Screen.height;
            const float speed = 3f;
            for (var delta = 0f; delta < 1; delta+=Time.deltaTime*speed)
            {
                t.localPosition = Vector3.Lerp(sourcePosition, targetPosition, delta);
                yield return null;
            }
            t.localPosition = targetPosition;
        }
    }
}
