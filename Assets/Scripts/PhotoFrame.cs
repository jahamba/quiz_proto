using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Random = UnityEngine.Random;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;


public class PhotoFrame : MonoBehaviour, IPointerDownHandler,IPointerUpHandler
{
    private List<PhotoItem> _photoItems;
    // [SerializeField]private List<PhotoItem> allPhotoItems;

    [SerializeField] private GameObject photoViewPrefab;
    [SerializeField] private GameObject numbersViewPrefab;
    [SerializeField] private Transform numbersFrame;
    [SerializeField] private BuyButton buyButton;
    
    private float _frameWidth;

    private List<PhotoView> _photoViews;

    private List<NumberView> _numberViews;
    private int _currentNumberView;

    private float _velocity = 0;
    private int _leftIndex;
    private int _rightIndex;
    
    
    
    private void Start()
    {
        _frameWidth = GetComponent<RectTransform>().rect.width;
        InitPhotoItems();
        ShufflePhotoItems();
        InitPhoto();
        InitNumbersFrame();
    }

    private void InitNumbersFrame()
    {
        const int numbersCount = 6;
        _numberViews = new List<NumberView>();
        _currentNumberView = 0;
        for (var i = 0; i < numbersCount; i++)
        {
            _numberViews.Add(Instantiate(numbersViewPrefab, numbersFrame).GetComponent<NumberView>());
        }
    }
    
    private void InitPhoto()
    {
        _photoViews = new List<PhotoView>();
        _frameWidth = GetComponent<RectTransform>().rect.width;

        for (var i = 0; i < 3; i++)
        {
            var photoViewGameObject = Instantiate(photoViewPrefab, transform);
            photoViewGameObject.transform.localPosition = Vector3.right * (i - 1) * _frameWidth;
            var photoView = photoViewGameObject.GetComponent<PhotoView>();
            photoView.SetPhotoItem(_photoItems[i]);
            photoView.index = i;
            _photoViews.Add(photoView);
        }
    }
    private void InitPhotoItems()
    {
        var allPhotoItems = 
            Resources.LoadAll<Sprite>("Photos").Select(sprite => new PhotoItem(sprite)).ToList();
        _photoItems = allPhotoItems.OrderBy(x => Random.value).ToList();
    }

    private void ShufflePhotoItems() =>
        _photoItems = _photoItems.OrderBy(x => Random.value).ToList();
    
    
    public void OnPointerDown(PointerEventData eventData)
    {
        StartCoroutine(CoroutineFunction());

        IEnumerator CoroutineFunction()
        {

            var mousePositionX = MousePositionX();
            var mousePositionY = MousePositionY();
            
            var photonViewTransforms = _photoViews.Select(photoView => photoView.transform).ToList();

            var initialized = false;
            var isHorizontal = false;

            const float selectLimit = -50f;
            
            while (true)
            {
                yield return null;
                if (!initialized)
                {
                    if (Mathf.Abs(mousePositionY - MousePositionY()) > 2)
                    {
                        isHorizontal = false;
                        initialized = true;
                    }
                    if (Mathf.Abs(mousePositionX - MousePositionX()) > 2)
                    {
                        isHorizontal = true;
                        initialized = true;
                    }
                }
                
                if(initialized)
                {
                    if(isHorizontal)
                    {
                        _velocity = MousePositionX() - mousePositionX;
                        mousePositionX = MousePositionX();
                        foreach (var t in photonViewTransforms)
                        {
                            t.localPosition += Vector3.right * _velocity;
                        }
                    }
                    else
                    {
                        var delta = MousePositionY() - mousePositionY;
                        mousePositionY = MousePositionY();
                        
                        var t = _photoViews[1].transform;
                        t.localPosition += Vector3.up * delta;
                        t.localPosition = new Vector3(
                            t.localPosition.x,
                            Mathf.Clamp(t.localPosition.y, selectLimit, 0));
                        
                        if (t.localPosition.y <= selectLimit)
                        {
                            yield return StartCoroutine(SelectCoroutineFunction());

                            _numberViews[_currentNumberView].SetNumber(_photoViews[1].photoItem.Number);
                            _currentNumberView++;

                            if (_currentNumberView >= _numberViews.Count)
                            {
                                buyButton.Show();
                                foreach (var numberView in _numberViews)
                                {
                                    numberView.SetEmpty();
                                }

                                _currentNumberView = 0;
                                InitPhotoItems();
                            }

                            t.localScale = Vector3.one;
                            t.localPosition = Vector3.right *_frameWidth;
                            
                            _photoItems.RemoveAt(_photoViews[1].index);
                            ShufflePhotoItems();
                            for (var i = 0; i < _photoViews.Count; i++)
                            {
                                _photoViews[i].index = Mathf.Min(i, _photoItems.Count - 1);
                                _photoViews[i].SetPhotoItem(_photoItems[_photoViews[i].index]);
                            }
                            
                            yield break;
                        }
                    }
                }
            }
        }

        float MousePositionX()
        {
            return Input.mousePosition.x * 390 / Screen.width; 
        }
        float MousePositionY()
        {
            return Input.mousePosition.y * 844 / Screen.height; 
        }

        IEnumerator SelectCoroutineFunction()
        {
            var t = _photoViews[1].transform;
            var sourcePosition = t.position;
            var targetPosition = _numberViews[_currentNumberView].transform.position;

            var sourceScale = Vector3.one;
            var targetScale = sourceScale * 0.15f;
            const float speed = 5f;
            for (var delta = 0f; delta < 1; delta += Time.deltaTime * speed)
            {
                t.position = Vector3.Lerp(sourcePosition, targetPosition, delta);
                t.localScale = Vector3.Lerp(sourceScale, targetScale, delta);
                yield return null;
            }

            t.position = targetPosition;
            
        }
        
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        StopAllCoroutines();
        
        var first = _photoViews.First().transform;
        var firstIndex = Mathf.RoundToInt(first.localPosition.x / _frameWidth);
        if (Mathf.Abs(_velocity) > 1f)
        {
            var dir = firstIndex - first.localPosition.x / _frameWidth;
            firstIndex += _velocity > 0 ? (dir > 0 ? 0 : 1) : (dir < 0 ? 0 : -1);
        }
        
        for (var i = 0; i < _photoViews.Count; i++)
        {
            StartCoroutine(CoroutineFunction(i));
        }

        IEnumerator CoroutineFunction(int index)
        {
            const float speed = 7f;
            var content = _photoViews[index].transform;
            var sourcePosition = content.localPosition;
            var targetPosition = Vector3.right * (firstIndex + index) * _frameWidth;

            var sourceScale = content.localScale;
            var targetScale = Vector3.one;
            
            for (float delta = 0; delta < 1; delta += Time.deltaTime * speed)
            {
                content.localPosition = Vector3.Lerp(sourcePosition, targetPosition, delta);
                content.localScale = Vector3.Lerp(sourceScale, targetScale, delta);
                yield return null;
            }
            content.localPosition = targetPosition;
            content.localScale = targetScale;

            if (firstIndex == 0 && index == _photoViews.Count - 1)
            {
                MoveItem(false);
            }

            if (firstIndex == -(_photoViews.Count - 1) && index == 0)
                MoveItem(true);
        }

        void MoveItem(bool firstToLast)
        {
            var fromIndex = firstToLast ? 0 : _photoViews.Count - 1;
            
            var item = _photoViews[fromIndex];
            
            _photoViews.RemoveAt(fromIndex);
            if (firstToLast)
                _photoViews.Add(item);
            else
                _photoViews.Insert(0, item);
            item.transform.localPosition = Vector3.right * (firstToLast ? _photoViews.Count -2 : -1) * _frameWidth;

            var photoItemIndex = firstToLast
                ? (_photoViews[_photoViews.Count - 2].index + 1) % _photoItems.Count
                : (_photoViews[1].index + _photoItems.Count - 1) % _photoItems.Count;

            item.SetPhotoItem(_photoItems[photoItemIndex]);
            item.index = photoItemIndex;
        }

        _velocity = 0;
    }

    
}
