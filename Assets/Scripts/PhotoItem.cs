using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhotoItem
{
    public readonly Sprite Sprite;
    public readonly int Number;

    public PhotoItem(Sprite sprite)
    {
        this.Sprite = sprite;
        this.Number = int.Parse(sprite.name.Split('_')[1]);
    }
}
